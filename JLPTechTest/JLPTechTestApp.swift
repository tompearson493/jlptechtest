//
//  JLPTechTestApp.swift
//  JLPTechTest
//
//  Created by Tom Pearson on 30/09/2022.
//

import SwiftUI

@main
struct JLPTechTestApp: App {
    var body: some Scene {
        WindowGroup {
            ProductsSearchView()
                .font(.jlpBody)
                .onAppear {
                    UINavigationBar.appearance().largeTitleTextAttributes = [.font: UIFont.jlpLargeNavigationBarTitleFont]
                    UINavigationBar.appearance().titleTextAttributes = [.font: UIFont.jlpNavigationBarTitleFont]
                    UIPageControl.appearance().currentPageIndicatorTintColor = .black
                    UIPageControl.appearance().pageIndicatorTintColor = .black.withAlphaComponent(0.2)
                }
        }
    }
}
