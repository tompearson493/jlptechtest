//
//  ProductDetailViewModel.swift
//  JLPTechTest
//
//  Created by Tom Pearson on 01/10/2022.
//

import Foundation

@MainActor
final class ProductDetailViewModel: ObservableObject {
    
    @Published private(set) var state: ViewState<Product> = .loading
    
    private let productsStore: ProductsStoreProcotol
    
    init(productsStore: ProductsStoreProcotol = Stores.shared.productsStore) {
        self.productsStore = productsStore
    }
    
    func loadProduct(_ id: String) async {
        state = .loading
        
        do {
            let product = try await productsStore.fetchProduct(id)
            state = .loaded(data: product)
        } catch {
            state = .error(error: error)
        }
    }
}
